// index.js

// JSON
/*
	Javascript Object Notation is a popular data format for application to communicate with each other. It may look like a Javascript Object but it is actually a string.

	JSON Syntax
	{
		"key1": "valueA",
		"key2": "valueB"
	}

	Keys are wrapped in curly braces
*/

// Object in JSON

let sample1 = `

{
	"name": "Katniss Everdeen",
	"age": 20,
	"address": {
		"city":"Kansas City",
		"state": "Kansas"
	}
}

`;

console.log(sample1);

//JSON.parse() - will return the JSON as a JS Object

console.log(JSON.parse(sample1));


// Array in JSON

let sampleArr = `
	[
		{
			"email": "jasonNewsted@gmail.com",
			"password": "iplaybass1234",
			"isAdmin": false
		},
		{
			"email": "larsDrums@gmail.com",
			"password": "metallicaMe80",
			"isAdmin": true
		}

	]
`;

console.log(sampleArr);

let parsedSampleArr = JSON.parse(sampleArr);
console.log(parsedSampleArr);

// JSON.parse does not mutate the original JSON format
console.log(sampleArr); 

console.log(parsedSampleArr.pop());
console.log(parsedSampleArr);




/*
If for example we need to send this data back to the client/front end, it should be in JSON format.
*/

//JSON.stringify() - will stringify JS objects as JSON
sampleArr = JSON.stringify(parsedSampleArr);
console.log(sampleArr);


/*
	Mini-Activity
	1. Given a JSON array, process it and convert to a JS Object.
	2. Delete the last item in the new JSON array.
	3. Add a new item in the JSON array.
	4. Then, stringify the array back to JSON.
*/

let jsonArr = `
	
	[
		"pizza",
		"hamburger",
		"spaghetti",
		"shanghai",
		"hotdog stick on a pineapple",
		"pancit bihon"
	]
`;
console.log(jsonArr);
let parsedJson = JSON.parse(jsonArr);
console.log(parsedJson);
console.log(parsedJson.pop());
console.log(parsedJson.push("cake"));

jsonArr = JSON.stringify(parsedJson);
console.log(jsonArr);


/* ANSWER

console.log(jsonArr);
let parsedJsonArr = JSON.parse(jsonArr);

parsedJsonArr.pop();
parsedJsonArr.push("ice cream");

jsonArr = JSON.stringify(parsedJsonArr);

console.log(jsonArr);

*/